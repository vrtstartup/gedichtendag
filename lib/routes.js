Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
});

Router.route('/', {
        name: 'home',
        waitOn: function() {

        },
        data: function () {
            var self = this;
            return {
                phrases: Phrases.find({}, {sort: {created: 1}}),
                admin: (self.params.query && self.params.query.rijmenenzwijnen) ? true : false
            }
        }
    }
);

Router.route('/login', {
    name: 'login'
});
