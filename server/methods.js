Meteor.methods({
  'add': function (text) {
    if (!text) throw new Meteor.Error(100, 'Je moet wel een zin ingeven he.');
    if(text.length > 140) throw new Meteor.Error(100, 'Je zin is te lang. Je mag maximaal 140 tekens typen.');
    if(text.length < 10) throw new Meteor.Error(100, 'Je zin is te kort. Je moet minstens 10 tekens typen.');
    check(text, String);

    var user = Meteor.user();

    if(user.lastEntry && (new Date() - user.lastEntry < 30*1000) ) {
      throw new Meteor.Error(100, 'Je kan maar twee zinnen per minuut plaatsen. Je moet nog ' + Math.round((new Date() - user.lastEntry)/1000) + ' seconden wachten.');

    }


    Phrases.insert({
      text: text,
      created: new Date(),
      userId: user.services.facebook.id,
      userName: user.profile.name
    });


    Meteor.users.update({ _id: Meteor.userId() }, { $set: { lastEntry: new Date() }});
    return true;

  },
  'remove': function (id) {
    if(Meteor.userId()) {
      Phrases.remove(id);
    }
  }
});
