Template.home.events({
  'submit form': function(e) {
    e.preventDefault();
    Meteor.call('add', $('input[type=text]').val(),  function(err, res ) {
      if(err) {
        swal("Oops...", err.reason, "error");
      } else {
        $('input[type=text]').val('');
        setTimeout(function() {
          var element = $('.phrases');
          $('.phrases').scrollTop($('.phrases').get(0).scrollHeight);
        }, 500);

      }
    });
  },
  'click button': function(e) {
    var t = $(e.currentTarget);
    Meteor.call('remove', t.data('id'));
  },
  'click .fb': function(e) {
    e.preventDefault();
    Meteor.loginWithFacebook();
  }
});

Template.home.helpers({
  'user': function() {
    return (Meteor.userId()) ? true : false
  },
  count: function() {
    return Phrases.find().fetch().length + 1;
  },
  firstname: function(name) {
    var a = name.split(' ');
    if(a.length > 1) {
      return a[0];
    } else {
      return name;
    }
  }
})

Template.home.rendered = function() {
  $('input[type=text]').focus();

  var scrolled = false;
  function updateScroll(force){
    if(!scrolled || force){
      var element = $('.phrases');
      $('.phrases').scrollTop($('.phrases').get(0).scrollHeight);
    }
  }

  $(".phrases").on('scroll', function(){

    if($('.phrases').scrollTop() + $('.phrases').height() == $('.phrases').get(0).scrollHeight) {
      scrolled = false;
    } else {
      scrolled=true;
    }
  });


  Tracker.autorun(function() {
    updateScroll(true);
  });



  setInterval(function() {
    updateScroll(false);
  }, 250);

  setTimeout(function() {
    updateScroll(true);
  }, 1000);

  $('.btn-facebook').bind('click', function(e) {
    FB.ui({
      method: 'share',
      href: 'http://gedichten.speeltuin.sambal.be/og.html'
    });
  });

  $('.btn-twitter').bind('click', function(e) {
    var txt = 'Schrijf mee aan het langste gedicht ooit:';
    var url = "http://www.een.be/programmas/een/schrijf-mee-aan-het-langste-gedicht-ooit";

    window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(txt) + "&url=" + encodeURIComponent(url), 'twshare', 'width=550,height=350');


  });

}
